package itis.parsing;

/*
Вы пишите систему для интеграции с готовой структурой заказчика и обработки данных о парках и зонах отдыха.
!!КЛАСС PARK менять НЕЛЬЗЯ (совсем). Считается что он является частью готовой библиотеки!!

 * В папке resources находятся два файла которые содержат примеры данных, которые нужно обрабатывать.
 * Данные записаны в формате одна строка - одно поле. Название поля и значение разделяются символом ":".
 * Над некоторыми полями стоят аннотации, которые служат для проверки даннных поля. Подробное описание доступно в классе аннотации.
___________________
 * Напишите код, который превратит содержимое файлов в обьекты класса "Park", учитывая аннотации над полями.
 * Парсинг должен учитывать следующие моменты:
 * 1. Парсятся значения только известных полей (которые есть в классе или упомянуты в аннотациях), все остальное игнорируется
 * 2. "null" должен распознаваться как null
 * 3. Ограничения аннотаций должны обрабатываться с помощью рефлексии
 * 4. Если файл не прошел проверку, должна формироваться ошибка с информативным содержанием (см. ниже)
___________________
 Обработка ошибок:
 Если при попытке парсинга файла возникли ошибки проверки (не соблюдаются ограничения аннотаций), должно выбрасываться ParkParsingException.
 Внутри должны быть перечислены все поля, при обработке которых возникли ошибки, и описание ошибки (напр. "Поле не может быть пустым")
 См. класс "ParkParsingException" для доп. информации.
___________________
Обработка аннотаций и формирование ошибок оцениваются отдельно.
 * */
import itis.parsing.annotations.FieldName;
import itis.parsing.annotations.MaxLength;
import itis.parsing.annotations.NotBlank;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class ParkParsingServiceImpl implements ParkParsingService {

    //Парсит файл в обьект класса "Park", либо бросает исключение с информацией об ошибках обработки
    @Override
    public Park parseParkData(String parkDatafilePath) throws ParkParsingException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {

        //write your code here
        File file = new File(parkDatafilePath);

        FileReader fileReader = null;
        try {
            fileReader = new FileReader(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        List<String> strings = bufferedReader.lines()
                .collect(Collectors.toList());

        Class parkClass = Park.class;
        Park park;

        List<ParkParsingException.ParkValidationError> parkValidationErrors = new ArrayList<>();
        try {
            Constructor declaredConstructor = parkClass.getDeclaredConstructor();
            declaredConstructor.setAccessible(true);
            park = (Park)declaredConstructor.newInstance();

            Field[] declaredFields = parkClass.getDeclaredFields();
            Arrays.asList(declaredFields).forEach(field -> {
                field.setAccessible(true);
                Arrays.asList(field.getDeclaredAnnotations()).forEach(annotation -> {
                    if (annotation.annotationType() == FieldName.class) {
                        String fieldName = field.getAnnotation(FieldName.class).value();


                        strings.forEach(s -> {

                            if (s.contains(fieldName)){

                                try {
                                    if(s.contains("null"))
                                        field.set(park, null);

                                    else if (field.getName().contains("Date")) {
                                        SimpleDateFormat sdf;
                                        sdf = new SimpleDateFormat("yyyy-MM-dd");
                                        try {
                                            Date date = sdf.parse(s.split("\"")[2]);
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    else
                                        field.set(park, s.split(" ")[1]);
                                } catch (IllegalAccessException e) {
                                    e.printStackTrace();
                                }
                            }
                            else
                                parkValidationErrors.add(new ParkParsingException.ParkValidationError(fieldName,"No such field at file"));
                        });
                    }

                    if (annotation.annotationType() == NotBlank.class){
                        try {
                            if (field.get(park)==null)
                                parkValidationErrors.add(new ParkParsingException.ParkValidationError(field.getName(),"Null value in field with @NotBlank annotation"));
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                    }

                    if (annotation.annotationType()== MaxLength.class){
                        try {
                            if (field.get(park).toString().length()>field.getAnnotation(MaxLength.class).value())
                                parkValidationErrors.add(new ParkParsingException.ParkValidationError(field.getName(),"Value of field is too long"));
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                    }
                });
            });

        } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            throw e;
        }

        if (!parkValidationErrors.isEmpty()) {
            ParkParsingException parkParsingException = new ParkParsingException("", parkValidationErrors);
            parkParsingException.printStackTrace();
            throw parkParsingException;
        }

        return park;
    }
}
