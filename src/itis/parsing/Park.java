//11:50

package itis.parsing;

import itis.parsing.annotations.FieldName;
import itis.parsing.annotations.MaxLength;
import itis.parsing.annotations.NotBlank;

import java.time.LocalDate;

//Вносить изменения в этот класс не нужно!
final class Park {

    @FieldName("title")//значение поля из другого поля
    private String legalName;

    @NotBlank//поле не пусто не нул
    @MaxLength(13)//макс длина
    private String ownerOrganizationInn;

    @NotBlank
    private LocalDate foundationYear;

    private Park() {
    }

    @Override
    public String toString() {
        return "Park{" +
                "legalName='" + legalName + '\'' +
                ", ownerOrganizationInn='" + ownerOrganizationInn + '\'' +
                ", foundationYear=" + foundationYear +
                '}';
    }
}
