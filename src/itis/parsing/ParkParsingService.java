package itis.parsing;

import java.lang.reflect.InvocationTargetException;

interface ParkParsingService {

    Park parseParkData(String parkDatafilePath) throws ParkParsingException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException;

}
